

CREATE TABLE  user_registration
(
    id INTEGER PRIMARY KEY AUTOINCREMENT,
  
     firstname TEXT,
    lastname TEXT,
   
    gender TEXT,
    date_of_birth INTEGER,
    
    normal_OR_Premium_user TEXT,
  
    address TEXT,
    
    phone_number INTEGER,
  
    created_on TEXT,
  
    Modified_on TEXT,
   
    created_by TEXT,
   
   Modified_by TEXT,
  
  Status INTEGER);
    
 
   
CREATE TABLE  book_details
(
    id INTEGER PRIMARY KEY AUTOINCREMENT,
 
   book_title TEXT,
    book_author TEXT,
   
   book_publisher TEXT,
  
   book_genre TEXT,
  
   released TEXT,
  
   Rating INTEGER,
  
   created_on TEXT,
   
   Modified_on TEXT,
   
   created_by TEXT,
   
   Modified_by TEXT,
   
   Status INTEGER);
    
 
 
  CREATE TABLE  authordetails
(
    id INTEGER PRIMARY KEY AUTOINCREMENT,
 
   Author_name TEXT,
    date_of_birth INTEGER,
 
   Description TEXT,
    created_on TEXT,
   
 Modified_on TEXT,
    created_by TEXT,
    Modified_by TEXT,
    Status INTEGER
    );
   

 
 
   
    CREATE TABLE  Publisherdetails
(
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    publisher_name TEXT,
   
 Established_on TEXT,
    Address TEXT,
    Description TEXT,
  
  created_on TEXT,
    Modified_on TEXT,
    created_by TEXT,
   
 Modified_by TEXT,
    Status INTEGER
    );


INSERT INTO authordetails (Author_name,date_of_birth, Description)
  
  VALUES ("ARHISI RAJ","03-2-1993","XYZZ");


SELECT *FROM authordetails;




INSERT INTO Publisherdetails(publisher_name,Established_on,Address,Description)
 
   VALUES ("Bindu Das", "20-2-2007","SSPO","abss");


INSERT INTO Publisherdetails (publisher_name,Established_on, Address,Description)
  
  VALUES ("John Kelly", "20-2-2011","XYOP","kkk");


SELECT *FROM Publisherdetails ;





INSERT INTO book_details(book_title,book_author,book_publisher,book_genre,released,Rating)
 
 VALUES ("Gitanjali","Robindranath Tagore","T & T","Novel","13-4-1993",9.5);


SELECT *FROM book_details ;




INSERT INTO user_registration(firstname,lastname,gender,date_of_birth,normal_OR_Premium_user,address,phone_number)
 
   VALUES ("Ishita","Banerjee","F","20-2-1997","Premium","Desh Bandhu","900000000");


SELECT *FROM user_registration;



SELECT book_details.book_title as Rented,user_registration.firstname,
user_registration.lastname,user_registration.gender,
user_registration.date_of_birth,user_registration.address,user_registration.phone_number

FROM book_details
JOIN user_registration
ON book_details.id=user_registration.id;



SELECT book_details.book_title as Liked,user_registration.firstname,
user_registration.lastname,user_registration.gender,
user_registration.date_of_birth,user_registration.address,user_registration.phone_number

FROM book_details
JOIN user_registration
ON book_details.id=user_registration.id;




CREATE TABLE  Friends
(
    id INTEGER PRIMARY KEY AUTOINCREMENT,
  
     friend_of TEXT,
    firstname TEXT,
   
     lastname TEXT,
    gender TEXT,
   
     date_of_birth INTEGER,
   
     normal_OR_Premium_user TEXT,
  
     address TEXT,
  
     phone_number INTEGER,
   
     created_on TEXT,
    
     Modified_on TEXT,
   
     created_by TEXT,
   
      Modified_by TEXT,
    Status INTEGER);

   
INSERT INTO user_registration(firstname,lastname,gender,date_of_birth,normal_OR_Premium_user,address,phone_number)
  
  VALUES ("Ankita","Ojha","F","21-6-1998","non-Premium","Desh Bandhu RR","900001019");
   

 
INSERT INTO friends(friend_of,firstname,lastname,gender,date_of_birth,normal_OR_Premium_user,address,phone_number)
    
VALUES ("Ishita Banerjee","Ankita","Ojha","F","21-6-1998","normal","Desh Bandhu RR","900001019");



SELECT user_registration.firstname,user_registration.lastname,friends.firstname As Friend_firstname ,
friends.lastname,friends.gender,friends.date_of_birth,friends.address
 
From user_registration
JOIN friends 
ON user_registration.id=friends.id;



INSERT INTO book_details(book_title,book_author,book_publisher,book_genre,released,Rating)
 
 VALUES ("The Catcher in the Rye","J D Salinger","T & S","Fiction","13-4-1998",9);
  

INSERT INTO book_details(book_title,book_author,book_publisher,book_genre,released,Rating)

  VALUES ("Ulysses","ARomes Joyce","T & K","Story","13-4-1900",7.5);


  INSERT INTO book_details(book_title,book_author,book_publisher,book_genre,released,Rating)
 
 VALUES ("Mysteryy","J D Salinger","T & S","Mystrey","13-4-1998",3);
  

INSERT INTO book_details(book_title,book_author,book_publisher,book_genre,released,Rating)
  
VALUES ("Dollkeeper","J D Salinger","T & S","Horror","13-4-1998",6);
 

 INSERT INTO book_details(book_title,book_author,book_publisher,book_genre,released,Rating)
  
VALUES ("Thakumar Jhuli","Sukumar Roy","T & S","Short story","13-4-1998",7);
 

 

SELECT user_registration.firstname,user_registration.lastname,book_details.book_title
 As Wishlist
From user_registration
JOIN book_details
ON user_registration.id<book_details.id;



SELECT user_registration.firstname,user_registration.lastname,user_registration.gender,user_registration.normal_OR_Premium_user 
FROM user_registration WHERE normal_OR_Premium_user="non-Premium";

SELECT user_registration.firstname,user_registration.lastname,user_registration.gender,user_registration.normal_OR_Premium_user
 FROM user_registration WHERE gender="F";



SELECT book_genre FROM book_details;
SELECT book_title,Rating FROM book_details WHERE Rating>4;



SELECT MAX(Rating) FROM book_details;



SELECT MIN(Rating) FROM book_details;



SELECT Author_name FROM authordetails WHERE Author_name="AR";


SELECT Established_on FROM Publisherdetails WHERE Established_on;



SELECT friends.firstname FROM Friends 
JOin user_registration
ON user_registration.id=1;



SELECT Established_on FROM Publisherdetails WHERE Established_on<2012 AND Established_on>2018;
 







